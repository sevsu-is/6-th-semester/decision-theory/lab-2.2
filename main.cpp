#include <iostream>

const int n = 7;
int A[n][n] = {
        {1, 0, 1, 0, 0, 1, 0},
        {1, 1, 1, 0, 1, 1, 0},
        {1, 0, 1, 0, 0, 1, 0},
        {0, 1, 0, 1, 1, 0, 1},
        {1, 1, 1, 0, 1, 1, 0},
        {1, 0, 1, 0, 0, 1, 0},
        {0, 1, 0, 1, 1, 0, 1}
};
int Matr_ekvival[n][n], Matr_predp[n][n], R[n][n];
int i, j;

int main() {
    //Формируем матрице эквивалентности
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (A[i][j] == 1 && A[j][i] != 1) {
                Matr_ekvival[i][j] = 0;
            } else
                Matr_ekvival[i][j] = A[i][j];
        }
    }

    //Формируем матрицу предпочтения
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (A[i][j] == 1 && A[j][i] == 1) {
                Matr_predp[i][j] = 0;
                Matr_predp[j][i] = 0;
            } else
                Matr_predp[i][j] = A[i][j];
        }
    }

    //Печатаем матрицу эквивалентности
    std::cout << "Матрица эквивалентности:" << std::endl;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            std::cout << Matr_ekvival[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    //Печатаем матрицу предпочтения
    std::cout << "Матрица строгого предпочтения:" << std::endl;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            std::cout << Matr_predp[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    //Формируем классы эквивалентности R
    int K[n][n];
    int unique[n];
    int count = 0;

    for (i = 0; i < n; i++) {
        unique[i] = 1;
    }

    std::cout << "Классы эквивалентности R:" << std::endl;
    for (int i = 0; i < n; i++) {
        std::cout << "R(x" << (i + 1) << ") = {";
        for (int j = 0; j < n; j++)
            if (Matr_ekvival[i][j] == 1)
                std::cout << " x" << (j + 1) << ",";
        std::cout << "}" << std::endl;
    }
    std::cout << std::endl;

    for (int i = 0; i < n; i++) {
        if (unique[i] == 1) {
            for (int j = 0; j < n; j++) {
                K[count][j] = Matr_ekvival[i][j];
                if ((j != i) && (Matr_ekvival[i][j] == 1))
                    unique[j] = 0;
            }
            count++;
        }
    }

    int matr_k[n][n];
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            matr_k[i][j] = 0;
        }
    }
    //Формируем классы эквивалентности K
    int l = 1;
    std::cout << "Множество X/~ неповторяющихся классов эквивалентности:" << std::endl;
    for (i = 0; i < n; i++) {
        if (unique[i] == 1) {
            std::cout << "k" << l << " -> {";
            l++;
            for (j = 0; j < n; j++)
                if (Matr_ekvival[i][j] == 1) {
                    std::cout << " x" << (j + 1) << ",";
                }
            std::cout << "}" << std::endl;
            for (j = 0; j < n; j++) {
                if (Matr_ekvival[i][j] == 1) {
                    matr_k[l - 2][j] = 1;
                }
            }
        }
    }
    std::cout << std::endl;

    std::cout << std::endl << "Матрица классов" << std::endl;
    for(int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << matr_k[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    int p = l - 1;

    int **matr_predp_klassow = new int *[p];
    for (int co = 0; co < p; co++)
        matr_predp_klassow[co] = new int[p];

    for (int i = 0; i < p; i++)
        for (int l = 0; l < p; l++)
            matr_predp_klassow[i][l] = 0;

    int index_i, index_j;
    int number_row = 0, numder_column = 0;
    int predp_klass = -1, ne_predp_klass = -1;

    //Строим матрицу предпочтения для классов эквивалентности
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (Matr_predp[i][j] == 1) {
                number_row = i;
                numder_column = j;

                for (int c = 0; c < n; c++) {
                    if (matr_k[c][number_row] == 1) {
                        predp_klass = c;
                    }
                    if (matr_k[c][numder_column] == 1) {
                        ne_predp_klass = c;
                    }

                    if (predp_klass != -1 && ne_predp_klass != -1) {
                        matr_predp_klassow[predp_klass][ne_predp_klass] = 1;
                        predp_klass = -1;
                        ne_predp_klass = -1;
                    }
                }
            }
        }
    }

    // Матрица предпочтений для классов
    std::cout << "Матрица предпочтений для классов:" << std::endl;
    for (i = 0; i < p; i++) {
        for (j = 0; j < p; j++) {
            std::cout << matr_predp_klassow[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    setlocale(LC_ALL, "Rus");

    int e = 0;
    int *U_K = new int[p]; // Выделение памяти для массива
    int temp = 0;
    U_K[0] = 0;
    int m = 0;
    int s = 0;
    int temp1 = 0, k;

    for (i = 1; i < p; i++) {
        U_K[i] = p + 1;
    }


    for (i = 0; i < p; i++) {
        //Проходим по строке
        for (j = m; j < p; j++) {
            if (matr_predp_klassow[i][j] != 0) {
                if (U_K[j] != p + 1 && U_K[i] == p + 1) {
                    U_K[i] = j + 1;
                    temp = temp++;
                    std::cout << "first" << U_K[i] << std::endl;

                }
            }
        }
        // Проходим по столбцу
        for (j = m; j < p; j++) {
            if (matr_predp_klassow[j][i] != 0) {
                if (U_K[i] != p + 1 && U_K[j] == p + 1) {
                    U_K[j] = i + 1;
                    m++;
                    e = m - 1;
                }
                // Проходим по строке в которой оказалась 1 из столбца
                for (k = i; k < p; k++) {
                    for (j = e; j < p; j++) {
                        if (matr_predp_klassow[k][j] != 0) {
                            if (U_K[j] == p + 1) {
                                U_K[j] = -(j + 1);
                            }
                        }
                    }
                }
            }
        }
    }

    std::cout << std::endl;
    for (k = 0; k < p; k++) {
        std::cout << U_K[k] << std::endl;
    }

    std::cout << std::endl;
    int U_X[n];

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (matr_k[i][j] == 1) {
                U_X[j] = U_K[i];
            }
        }
    }

    for (k = 0; k < n; k++) {
        std::cout << U_X[k] << std::endl;
    }
}